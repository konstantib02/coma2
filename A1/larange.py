import sys

#Aufgabe 3a
# @param: nodes: stuetzstellen, 
#       k: k-tes Larangepolynom,        
#       x: Stelle an der wir k-tes L. Polynom auswerten wollen
# @return: Ausgewertetes L_k an x
def lagrange(x, nodes, k):
    n = len(nodes)
    # pruefen of k <= n, ansosnten Fehlermeldung
    if (k > n):
        print("Error: Index k must be less than the number of nodes.")
        sys.exit(1)
    L_k_x = 1
    for j in range(n):
        if j != k:
            L_k_x *= (x - nodes[j]) / (nodes[k] - nodes[j])
    return L_k_x

#if len(sys.argv) < 4:
    print("Usage: python script_name.py x nodes k")
    sys.exit(1)

# arguments zuweisen
#x = float(sys.argv[1])  # first argument (after script call)
#k = int(sys.argv[-1])    # last argument
#nodes = [float(arg) for arg in sys.argv[2:-1]]  # All arguments between x and k

#result1 = lagrange(x, nodes, k)
#print("For the given nodes, the value of the k-th Lagrange polynomial at x is: ", result1)

# Aufgabe 3b
# Berechnen einer Larange Interpolation mithilfe von lagrange()

# @param: x: Stelle an der wir das Polynom auswerten wollen
#       nodes: Stuetzstellen
#       function_values: Funktionswerte an den Stuetzstellen
# @return: Ausgewertetes Polynom an x
def lagrange_interpolation(x, nodes, function_values):
    n = len(nodes)
    polynom = 0
    for k in range(n):
        L_k_x = lagrange(x, nodes, k)
        polynom += function_values[k] * L_k_x
    return polynom

# arguments zuweisen
x = float(sys.argv[1])  # first argument (after script call)

if (len(sys.argv) - 2) % 2 != 0:
    print("Error: Stützstellenwerte und Funktionswerte müssen die gleiche Anzahl haben.")
    sys.exit(1)

n = (len(sys.argv) - 2) // 2  # berechne Anzahl der Stuetzstellen bzw Funktionswerte (skript call und x werden hier abgezogen)
nodes = [float(arg) for arg in sys.argv[2:2+n]]  # Weise die erste hälfte der args den nodes zu
function_values = [float(arg) for arg in sys.argv[2+n:]]  # weise zweite hälfte den Funktionswerten zu

result2 = lagrange_interpolation(x, nodes, function_values)
print("For the given nodes, the value of the Lagrange interpolation at x is: ", result2)